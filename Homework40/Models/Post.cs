﻿using Homework40.Interfaces;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework40.Models
{
    public class Post : IEntity<int>
    {
        public int ID { get; set; }

        [ForeignKey("User")]
        public int UserID { get; set; }
        public string Comment { get; set; }
        public virtual User User { get; set; }
    }
}
